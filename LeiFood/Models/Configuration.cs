﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LeiFood.Models
{
    [XmlRoot("Root")]
    public class Configuration
    {
        [XmlElement("usuario")]
        public string usuario { get; set; }

        [XmlElement("senha")]
        public string senha { get; set; }

        [XmlElement("idFrn")]
        public string idFrn { get; set; }

        [XmlElement("pathXML")]
        public string pathXML { get; set; }

        [XmlElement("timer")]
        public string timer { get; set; }

        [XmlElement("mudo")]
        public bool mudo { get; set; }
    }
}
