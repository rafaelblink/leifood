﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LeiFood.Models
{
    [XmlRoot("itens_list")]
    public class Itens
    {
        [XmlElement("itens")]
        public List<Item> itens { get; set; }
    }
}
