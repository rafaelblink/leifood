﻿using LeiFood.Models;
using LeiFood.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace LeiFood
{
    public partial class FrmTray : Form
    {
        public List<Configuration> configurations;
        public string XML;
        public string completePath;
        public string completePathItens;

        private NotifyIcon trayIcon;
        private ContextMenu trayMenu;

        public FrmTray()
        {
            configurations = new List<Configuration>();
            VerifyFileSecurity();
            VerifyConfigurationFile();
            InitializeComponentTray();

            VerifyFilePedidos();

            timer1 = new Timer();
            timer1.Interval = int.Parse(configurations.FirstOrDefault().timer) * 1000;
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Enabled = true;
            timer1.Start();
        }

        private void VerifyFileSecurity()
        {
            //C:\Users\rafa\AppData\Local\VirtualStore\Windows 29-07-2016-l31f00d

            var path = @"C:\Windows\l31f00d";

            while (!File.Exists(path))
            {
                FrmPassword frmConfirm = new FrmPassword();
                frmConfirm.ShowDialog();
                var pass = FrmPassword.pass;

                string passDay = string.Format("{0}-{1}", DateTime.Now.ToString("dd/MM/yyyy"), "l31f00d");
                try
                {
                    if (pass.Equals(passDay))
                    {
                        File.Create(path);
                        MessageBox.Show("Arquivo criado com sucesso!", "Senha correta!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("A senha não confere, digite novamente a senha!", "Senha incorreta!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Você não tem permissão.", "Sem permissão", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }

        }

        private void VerifyWS()
        {
            try
            {
                foreach (var configuration in configurations)
                {
                    string urlVerify = string.Format("http://webservice.ifood.com.br/wspdv/api/selecionaPedidosAlteradosAPartirDe?login={0}&senha={1}&idFrn={2}&contentType=xml", configuration.usuario, configuration.senha, configuration.idFrn);
                    HttpWebRequest request = WebRequest.Create(urlVerify) as HttpWebRequest;
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(response.GetResponseStream());
                    XmlNodeList listStatus = xmlDoc.SelectNodes("//response-status");

                    if (listStatus[0].InnerText == "00")
                    {
                        var listNew = new List<Item>();
                        var listItensXML = DeserializeItensXML(completePathItens);
                        XmlNodeList list = xmlDoc.SelectNodes("//codPedido");

                        if (listItensXML.itens.Count == 0)
                        {
                            for (int i = 0; i < list.Count; i++)
                            {
                                string pathOrder = Path.Combine(configuration.pathXML, string.Format("{0}_Pedido_{1}_{2}.xml", configuration.idFrn, list[i].InnerXml, DateTime.Now.ToString("dd-MM-yyyy")));
                                string pathItens = Path.Combine(configuration.pathXML, string.Format("{0}_Itens_{1}_{2}.xml", configuration.idFrn, list[i].InnerXml, DateTime.Now.ToString("dd-MM-yyyy")));

                                string urlGetOrder = string.Format("http://webservice.ifood.com.br/wspdv/api/selecionaPedido?login={0}&senha={1}&idFrn={2}&contentType=xml&codPedido={3}", configuration.usuario, configuration.senha, configuration.idFrn, list[i].InnerXml);
                                Directory.CreateDirectory(configuration.pathXML);
                                CreateXMLOrder(urlGetOrder, pathOrder);

                                if (!configuration.mudo)
                                {
                                    SoundPlayer oneUp = new SoundPlayer(Resources._1_up);
                                    oneUp.Play();
                                }
                                alertInfoBalloon(string.Format("Novo pedido número: {0}", list[i].InnerXml));

                                string urlGetItens = string.Format("http://webservice.ifood.com.br/wspdv/api/selecionaItens?login={0}&senha={1}&idFrn={2}&contentType=xml&codPedido={3}", configuration.usuario, configuration.senha, configuration.idFrn, list[i].InnerXml);
                                CreateXMLItens(urlGetItens, pathItens);
                                listItensXML.itens.Add(new Item() { item = list[i].InnerXml });
                            }
                            SerializeItensXML(listItensXML.itens);

                        }
                        else
                        {
                            for (int i = 0; i < list.Count; i++)
                            {
                                if (listItensXML.itens.Where(t => t.item == list[i].InnerXml).ToList().Count == 0)
                                {
                                    string pathOrder = Path.Combine(configuration.pathXML, string.Format("{0}_Pedido_{1}_{2}.xml", configuration.idFrn, list[i].InnerXml, DateTime.Now.ToString("dd-MM-yyyy")));
                                    string pathItens = Path.Combine(configuration.pathXML, string.Format("{0}_Itens_{1}_{2}.xml", configuration.idFrn, list[i].InnerXml, DateTime.Now.ToString("dd-MM-yyyy")));

                                    string urlGetOrder = string.Format("http://webservice.ifood.com.br/wspdv/api/selecionaPedido?login={0}&senha={1}&idFrn={2}&contentType=xml&codPedido={3}", configuration.usuario, configuration.senha, configuration.idFrn, list[i].InnerXml);

                                    CreateXMLOrder(urlGetOrder, pathOrder);
                                    if (!configuration.mudo)
                                    {
                                        SoundPlayer oneUp = new SoundPlayer(Resources._1_up);
                                        oneUp.Play();
                                    }
                                    alertInfoBalloon(string.Format("Novo pedido número: {0}", list[i].InnerXml));

                                    string urlGetItens = string.Format("http://webservice.ifood.com.br/wspdv/api/selecionaItens?login={0}&senha={1}&idFrn={2}&contentType=xml&codPedido={3}", configuration.usuario, configuration.senha, configuration.idFrn, list[i].InnerXml);
                                    CreateXMLItens(urlGetItens, pathItens);

                                    listItensXML.itens.Add(new Item() { item = list[i].InnerXml });
                                }
                            }
                            SerializeItensXML(listItensXML.itens);
                        }
                    }
                    else
                    {
                        alertBalloon(string.Format("O dados do fornecedor {0} parecem estar invalidos, motivo: {1}", configuration.idFrn, "Dados de autenticação inválidos"));
                    }
                }
            }
            catch (Exception ex)
            {
                alertBalloon(ex.Message);
            }


            //try
            //{
            //    HttpWebRequest request = WebRequest.Create(urlVerify) as HttpWebRequest;
            //    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            //    XmlDocument xmlDoc = new XmlDocument();
            //    xmlDoc.Load(response.GetResponseStream());
            //    XmlNodeList listStatus = xmlDoc.SelectNodes("//response-status");

            //    if (listStatus[0].InnerText == "00")
            //    {
            //        var listNew = new List<Item>();
            //        var listItensXML = DeserializeItensXML(completePathItens);

            //        XmlNodeList list = xmlDoc.SelectNodes("//codPedido");

            //        if (listItensXML.itens.Count == 0)
            //        {
            //            for (int i = 0; i < list.Count; i++)
            //            {
            //                string pathOrder = Path.Combine(configuration.pathXML, string.Format("Pedido_{0}_{1}.xml", list[i].InnerXml, DateTime.Now.ToString("dd-MM-yyyy")));
            //                string pathItens = Path.Combine(configuration.pathXML, string.Format("Itens_{0}_{1}.xml", list[i].InnerXml, DateTime.Now.ToString("dd-MM-yyyy")));

            //                string urlGetOrder = string.Format("http://webservice.ifood.com.br/wspdv/api/selecionaPedido?login={0}&senha={1}&idFrn={2}&contentType=xml&codPedido={3}", configuration.usuario, configuration.senha, configuration.idFrn, list[i].InnerXml);
            //                Directory.CreateDirectory(configuration.pathXML);
            //                CreateXMLOrder(urlGetOrder, pathOrder);
            //                if (!configuration.mudo)
            //                {
            //                    SoundPlayer oneUp = new SoundPlayer(Resources._1_up);
            //                    oneUp.Play();
            //                }
            //                alertInfoBalloon(string.Format("Novo pedido número: {0}", list[i].InnerXml));

            //                string urlGetItens = string.Format("http://webservice.ifood.com.br/wspdv/api/selecionaItens?login={0}&senha={1}&idFrn={2}&contentType=xml&codPedido={3}", configuration.usuario, configuration.senha, configuration.idFrn, list[i].InnerXml);
            //                CreateXMLItens(urlGetItens, pathItens);

            //                listItensXML.itens.Add(new Item() { item = list[i].InnerXml });
            //            }

            //            SerializeItensXML(listItensXML.itens);
            //        }
            //        else
            //        {
            //            for (int i = 0; i < list.Count; i++)
            //            {
            //                if (listItensXML.itens.Where(t => t.item == list[i].InnerXml).ToList().Count == 0)
            //                {
            //                    string pathOrder = Path.Combine(configuration.pathXML, string.Format("Pedido_{0}_{1}.xml", list[i].InnerXml, DateTime.Now.ToString("dd-MM-yyyy")));
            //                    string pathItens = Path.Combine(configuration.pathXML, string.Format("Itens_{0}_{1}.xml", list[i].InnerXml, DateTime.Now.ToString("dd-MM-yyyy")));

            //                    string urlGetOrder = string.Format("http://webservice.ifood.com.br/wspdv/api/selecionaPedido?login={0}&senha={1}&idFrn={2}&contentType=xml&codPedido={3}", configuration.usuario, configuration.senha, configuration.idFrn, list[i].InnerXml);

            //                    CreateXMLOrder(urlGetOrder, pathOrder);
            //                    if (!configuration.mudo)
            //                    {
            //                        SoundPlayer oneUp = new SoundPlayer(Resources._1_up);
            //                        oneUp.Play();
            //                    }
            //                    alertInfoBalloon(string.Format("Novo pedido número: {0}", list[i].InnerXml));

            //                    string urlGetItens = string.Format("http://webservice.ifood.com.br/wspdv/api/selecionaItens?login={0}&senha={1}&idFrn={2}&contentType=xml&codPedido={3}", configuration.usuario, configuration.senha, configuration.idFrn, list[i].InnerXml);
            //                    CreateXMLItens(urlGetItens, pathItens);

            //                    listItensXML.itens.Add(new Item() { item = list[i].InnerXml });
            //                }
            //            }

            //            SerializeItensXML(listItensXML.itens);
            //        }

            //    }
            //    else
            //    {
            //        alertBalloon("Os dados de autenticação parecem estar inválidos!");
            //    }

            //}
            //catch (Exception e)
            //{
            //    //alertBalloon("A conexão com a internet ou o servidor do iFood parecem estar offlines, verifique sua conexão!");
            //    alertBalloon(e.Message);
            //}
        }

        private void alertBalloon(string msg)
        {
            trayIcon.BalloonTipText = msg;
            trayIcon.BalloonTipIcon = ToolTipIcon.Error;
            trayIcon.BalloonTipTitle = "LeiFood - Alerta";
            trayIcon.ShowBalloonTip(500);
        }
        private void alertInfoBalloon(string msg)
        {
            trayIcon.BalloonTipText = msg;
            trayIcon.BalloonTipIcon = ToolTipIcon.Info;
            trayIcon.BalloonTipTitle = "LeiFood - Alerta";
            trayIcon.ShowBalloonTip(500);
        }

        private void CreateXMLItens(string urlGetItens, string pathItens)
        {
            HttpWebRequest request = WebRequest.Create(urlGetItens) as HttpWebRequest;
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(response.GetResponseStream());
            try
            {
                xmlDoc.Save(pathItens);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private void CreateXMLOrder(string urlGetOrder, string pathOrder)
        {
            HttpWebRequest request = WebRequest.Create(urlGetOrder) as HttpWebRequest;
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(response.GetResponseStream());
            try
            {
                xmlDoc.Save(pathOrder);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private void InitializeComponentTray()
        {
            trayMenu = new ContextMenu();

            trayMenu.MenuItems.Add("Pausar", PauseTimer);
            //trayMenu.MenuItems.Add("Configurações", OpenConfigs);
            trayMenu.MenuItems.Add("Sair", OnExit);

            trayIcon = new NotifyIcon();
            trayIcon.Text = "LeiFood";
            trayIcon.Icon = new Icon(Resources.favico32, 40, 40);

            trayIcon.ContextMenu = trayMenu;
            trayIcon.Visible = true;
        }

        private void PauseTimer(object sender, EventArgs e)
        {
            timer1.Stop();

            for (int i = 0; i < trayMenu.MenuItems.Count; i++)
            {
                if (trayMenu.MenuItems[i].Text.Equals("Pausar"))
                {
                    trayMenu.MenuItems.RemoveAt(i);

                    MenuItem ContinueItem = new MenuItem("Continuar", ContinueTimer);
                    trayMenu.MenuItems.Add(i, ContinueItem);
                }
            }
        }

        private void ContinueTimer(object sender, EventArgs e)
        {
            timer1.Start();

            for (int i = 0; i < trayMenu.MenuItems.Count; i++)
            {
                if (trayMenu.MenuItems[i].Text.Equals("Continuar"))
                {
                    trayMenu.MenuItems.RemoveAt(i);

                    MenuItem PauseItem = new MenuItem("Pausar", PauseTimer);
                    trayMenu.MenuItems.Add(i, PauseItem);
                }
            }
        }

        private void OpenConfigs(object sender, EventArgs e)
        {
            //FrmConfiguration frmConfig = new FrmConfiguration();
            //frmConfig.ShowDialog();
            //configuration = FrmConfiguration.configuration;

            //SerializeXMLConfig(configuration, completePath);
        }

        protected override void OnLoad(EventArgs e)
        {
            Visible = false;
            ShowInTaskbar = false;
            base.OnLoad(e);
        }

        private void VerifyConfigurationFile()
        {
            string pathFileConfiguration = AppDomain.CurrentDomain.BaseDirectory;
            DirectoryInfo d = new DirectoryInfo(pathFileConfiguration);

            string[] files = Directory.GetFiles(pathFileConfiguration, "Configuration_*.xml");

            if (files.Count() > 0)
            {
                try
                {
                    foreach (var item in files)
                    {
                        var config = DeserializeConfig(item);
                        configurations.Add(config);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                completePath = Path.Combine(pathFileConfiguration, "Configuration_00.xml");
                MessageBox.Show("Não foi encontrado nenhum arquivo de configuração!\nSerá aberta uma janela com os campos para cadastro da configuração padrão.", "Arquivo não encontrado!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FrmConfiguration frmConfig = new FrmConfiguration();
                frmConfig.ShowDialog();
                var configuration = FrmConfiguration.configuration;
                configurations.Add(configuration);
                SerializeXMLConfig(configuration, completePath);
            }


            //string pathFileConfiguration = AppDomain.CurrentDomain.BaseDirectory;
            //string fileName = "Configuration.xml";

            //completePath = Path.Combine(pathFileConfiguration, fileName);

            //if (!File.Exists(completePath))
            //{
            //    MessageBox.Show("Não foi encontrado nenhum arquivo de configuração!\nSerá aberta uma janela com os campos para cadastro.", "Arquivo não encontrado!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    FrmConfiguration frmConfig = new FrmConfiguration();
            //    frmConfig.ShowDialog();
            //    configuration = FrmConfiguration.configuration;
            //    SerializeXMLConfig(configuration, completePath);
            //}
            //else
            //{
            //    DeserializeConfig(completePath);
            //    FrmConfiguration.configuration = configuration;
            //}
        }

        private void SerializeXMLConfig(Configuration config, string completePath)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Configuration));

            using (TextWriter writer = new StreamWriter(completePath))
            {
                serializer.Serialize(writer, config);
            }
        }

        private Configuration DeserializeConfig(string completePath)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(Configuration));
            TextReader reader = new StreamReader(completePath);
            object obj = deserializer.Deserialize(reader);

            var configuration = (Configuration)obj;
            reader.Close();

            return configuration;
        }



        private void OnExit(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            VerifyFilePedidos();
        }

        private void VerifyFilePedidos()
        {
            string pathFileConfiguration = AppDomain.CurrentDomain.BaseDirectory;
            completePathItens = Path.Combine(pathFileConfiguration, "List_Pedidos.xml");

            if (!File.Exists(completePathItens))
            {
                SerializeItensXML(new List<Item>());
                VerifyWS();
            }
            else
            {
                VerifyWS();
            }
        }

        private void SerializeItensXML(List<Item> ListItens)
        {
            Itens itens = new Itens();
            itens.itens = ListItens;

            XmlSerializer serializer = new XmlSerializer(typeof(Itens));
            FileStream fs = new FileStream(completePathItens, FileMode.Create);
            serializer.Serialize(fs, itens);
            fs.Close();
        }

        private Itens DeserializeItensXML(string completePath)
        {
            try
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(Itens));
                TextReader reader = new StreamReader(completePath);
                object obj = deserializer.Deserialize(reader);

                var itens = (Itens)obj;
                reader.Close();

                return itens;

            }
            catch (Exception)
            {
                throw new Exception("Falha ao recuperar dados dos itens, seu arquivo de itens parece estar corrompido.");
            }
            
        }
    }
}
