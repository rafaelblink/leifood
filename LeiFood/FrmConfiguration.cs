﻿using LeiFood.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeiFood
{
    public partial class FrmConfiguration : Form
    {
        public static Configuration configuration;
        //public static List<Configuration> configurations;

        public FrmConfiguration()
        {
            InitializeComponent();

            if (configuration != null)
                setTxtConfig();                        
        }

        private void SetConfig()
        {
            string usuario = txtUsuario.Text;
            string senha = txtSenha.Text;
            string idFrn = txtFornecedor.Text;
            string pathXML = txtPathXML.Text;
            string timer = numTimer.Value.ToString();
            bool mudo = chbMudo.Checked;

            configuration = new Configuration()
            {
                usuario = usuario,
                senha = senha,
                idFrn = idFrn,
                pathXML = pathXML,
                timer = timer,
                mudo = mudo
            };
        }

        private void setTxtConfig()
        {
            txtUsuario.Text = configuration.usuario;
            txtSenha.Text = configuration.senha;
            txtFornecedor.Text = configuration.idFrn;
            txtPathXML.Text = configuration.pathXML;
            numTimer.Value = decimal.Parse(configuration.timer);
            chbMudo.Checked = configuration.mudo;
        }

        private void txtSalvar_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                if (Directory.Exists(txtPathXML.Text))
                {
                    SetConfig();
                    MessageBox.Show("Configurações salvas com sucesso!", "Salvo com sucesso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    Close();
                }
                else
                {
                    MessageBox.Show("Diretorio digitado não existe ou não é válido!", "Erro de validação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
            }            
        }

        private bool ValidateForm()
        {
            bool status = true;

            if (txtUsuario.Text.Length == 0)
            {
                errorProvider1.SetError(txtUsuario, "O campo usuário é obrigatório.");
                status = false;
            }
            if (txtSenha.Text.Length == 0)
            {
                errorProvider1.SetError(txtSenha, "O campo senha é obrigatório.");
                status = false;
            }
            if (txtFornecedor.Text.Length == 0)
            {
                errorProvider1.SetError(txtFornecedor, "O campo fornecedor é obrigatório.");
                status = false;
            }
            if (txtPathXML.Text.Length == 0)
            {
                errorProvider1.SetError(txtPathXML, "O campo Path é obrigatório.");
                status = false;
            }

            return status;
        }
    }
}
