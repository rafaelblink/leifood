﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeiFood
{
    public partial class FrmPassword : Form
    {
        public static string pass;

        public FrmPassword()
        {
            InitializeComponent();
            pass = string.Empty;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            pass = txtPass.Text;

            if (!string.IsNullOrEmpty(pass))
            {
                Dispose();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
            Dispose();
            Close();
        }
    }
}
